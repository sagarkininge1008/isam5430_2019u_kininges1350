﻿using System;
namespace HW02
{
    /// <summary>
    /// A palindrome is a sequence of characters that reads the same backward
    /// as forward. For example, each of the following five-digit integers
    /// is a palindrome: 12321, 55555, 45554 and 11611. 
    /// Write an app that reads in exactly 9-digit positive integers 
    /// and determines whether it’s a palindrome (display either:
    /// "a palindrome" or "not a palindrome"). If the number is not 
    /// nine digits long, display an error message and allow the user to
    /// enter a new value. </summary>
    /// <remark>[Hint: Use the remainder and division operators to pick off
    /// the number’s digits one at a time, from right to left.]</remark>
    /// 
    public class IntegerPalindromes
    {
        public static void Main()
        {
           
          int n,r,sum=0,temp;    
          Console.WriteLine("Enter the Number: ");   
          n = int.Parse(Console.ReadLine());
          while(n.ToString().Length != 9){
            Console.WriteLine(" Enter 9 digits "); 
            n = int.Parse(Console.ReadLine());
           }
          temp=n;      
          while(n>0)      
          {      
           r=n%10;      
           sum=(sum*10)+r;      
           n=n/10;      
          }      
          if(temp==sum)      
           Console.WriteLine("Number is Palindrome.");      
          else      
           Console.WriteLine("Number is not Palindrome");     
        }
           
    }
}

