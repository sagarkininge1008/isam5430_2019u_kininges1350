﻿using System;
namespace HW02
{
    /// <summary>
    /// The process of finding the maximum value (i.e., the largest
    /// of a group of values) is used frequently in computer applications.
    /// For example, an app that determines the winner of a sales contest
    /// would input the number of units sold by each salesperson. The
    /// salesperson who sells the most units wins the contest.
    /// Write pseudocode, then a C# app that inputs a series of 10 integers,
    /// then determines and displays the largest integer followed by the
    /// second largest integer.
    /// Your app should use at least the following four variables:
    ///     counter: A counter to count to 10 (i.e., to keep track of
    ///        how many numbers have been input and to determine when all
    ///        10 numbers have been processed).
    ///     number (integer): The integer most recently input by the user.
    ///     largest: The largest number found so far.
    ///     largest2: The second largest number found so far.
    /// </summary>
    public class LargestNumbers
    {
        public static void Main()
        {
            
                int[] numbers = new int[10];
                int largest = int.MinValue;
                int second = int.MinValue;
                Console.WriteLine("Enter 10 Numbers : "); 
                for(int i =0;i<10;++i){
                   numbers[i] = int.Parse(Console.ReadLine()); 
                }
                foreach (int i in numbers)
                {
                if (i > largest)
                {
                second = largest;
                largest = i;
                 }
                else if (i > second)
                second = i;
                }
                System.Console.WriteLine("First largest: "+largest);
                System.Console.WriteLine("Second largest: "+second);
        }
    }
}
