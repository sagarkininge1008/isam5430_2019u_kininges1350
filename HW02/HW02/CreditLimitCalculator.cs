﻿using System;
using static System.Console;
namespace HW02
{
    /// <summary>
    /// Develop a C# app that will determine whether any of several
    /// department-store customers has exceeded the credit limit on a charge
    /// account. For each customer, the following facts are available:
    ///    1) account number,
    ///    2) balance at the beginning of the month,
    ///    3) total of all items charged by the customer this month,
    ///    4) total of all credits applied to the customer’s account this month and,
    ///    5) allowed credit limit.
    /// The app should input all these facts as integers,
    /// calculate the new balance(= beginning balance + charges – credits),
    /// display the new balance and determine whether the new balance exceeds
    /// the customer’s credit limit.
    /// For those whose credit limit is exceeded, 
    /// the app should display the message "Credit limit exceeded" on a separate line. 
    /// Use sentinel-controlled iteration to obtain the data for each account
    /// (e.g. the iteration will terminate if the account number is a
    /// negative number).
    /// </summary>
    public class CreditLimitCalculator
    {
        public static void Main()
        {
            
        int accountNumber;  
        int balance; 
        int totalItems; 
        int totalCredits;  
        int allowedCreditLimit;
        int newBalance;
        while(true){
        Console.WriteLine("Enter Account Number:");
        accountNumber = int.Parse(Console.ReadLine());
        if(accountNumber<=0) return;
        Console.WriteLine("Enter balance at the beginning of the month:");
        balance = int.Parse(Console.ReadLine());
        Console.WriteLine("Enter total of all items charged by the customer this month:");
        totalItems=int.Parse(Console.ReadLine());
        Console.WriteLine("Enter total of all credits applied to the customer’s account this month:");
        totalCredits=int.Parse(Console.ReadLine());
        Console.WriteLine("Enter allowed credit limit:");
        allowedCreditLimit=int.Parse(Console.ReadLine());
        newBalance = balance + totalItems - totalCredits;
        Console.WriteLine("Customer:"+accountNumber);
        Console.WriteLine("Balance at the beginning of the month:"+balance);
        Console.WriteLine("Total of all items charged by the customer this month:"+totalItems);
        Console.WriteLine("Total of all credits applied to the customer’s account this month:"+totalCredits);
        Console.WriteLine("Allowed credit limit:"+allowedCreditLimit);
        Console.WriteLine("New Balance:"+newBalance);
    
        if (newBalance < allowedCreditLimit) {
           Console.WriteLine("Credit limit exceeded");
        }
        }
        }
    }
}
