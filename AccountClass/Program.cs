﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountClass
{
    class Program
    {
        static void Main(String[] args)
        {
            // JS = Java = C# = C++
            Account account = new Account();
            // python: account = Account()
            account.GetName = "Mike";
            Console.WriteLine(account.GetName());
            account.SetName("Sagar");
            Console.WriteLine(account.GetName());
            Console.WriteLine($"account name = {account.Name}");
            Console.ReadLine();
        }
    }
}
