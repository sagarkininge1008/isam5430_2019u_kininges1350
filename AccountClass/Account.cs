﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountClass
{
    class Account
    {
        //data member: field
        //the name is encapsulated/hidden from outside
        private string _name;

        //public property
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        // C# will translate the property codes above to the following codes internally.
        /*
            public void set_Name(string value)
        {
            _name = value;
        }


        public string get_Name()
        {
            return _name;

        }
        */
        //Python
        //def SetName(self, name: str):
        // self._name = name
        //JS:
        //function SetName(name) {...
        //this.name = name;
        //}
        /// <summary>
        /// Sets the name into the variable
        /// </summary>
        /// <param name="name">the name</param>
        public void SetName(string name)
        {
            _name = name;

        }

        // def GetName(self):
        //   return self._name

        public string GetName()
        {
            return _name;
        }
    }
}
