﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISAM5430.HW03
{
    public class Arrays
    {
        #region Without Loops
        /// <summary>
        /// Given an array of ints, return true if 6 appears as either the first or last element in the array.The array will be length 1 or more.
        /// </summary>
        /// <example>firstLast6([6, 1, 2, 3]) → true</example>
        /// <example>firstLast6([1, 2, 6]) → true</example>
        /// <example>firstLast6([13, 6, 1, 2, 3]) → false</example>
        /// <param name="array"></param> 
        /// <returns>true if 6</returns>
        public bool FirstLast6(int[] array)
        {
            if (array== null)
                return false;
                if (array.Length>=1)
            {
                if((array[0]==6) || (array[array.Length - 1]==6))
                {
                    return true;
                }
                return false;
            }
            return false;         

           // throw new NotImplementedException();
        }

        /// <summary>
        /// Given an array and an index, return true if the sum of the first and the last element is equal to the element located at the position index in the array.
        /// </summary>
        /// <returns><c>true</c>, if equal was sumed, <c>false</c> otherwise.</returns>
        /// <param name="array">Array.</param>
        /// <param name="index">Index.</param>
        public bool SumEqual(int[] array, int index)
        {
            if (array == null)
                return false;
            if(index>0 && index<array.Length)
            {
                if (array[0] + array[array.Length - 1] == array[index])
                {
                    return true;
                }
                return false;
            }return false;
            //throw new NotImplementedException();
        }
        #endregion

        #region Single Loop
        /// <summary>
        /// Given start and end numbers, return a new array containing the sequence of integers from start up to but not including end, so start=5 and end=10 yields {5, 6, 7, 8, 9}. The end number will be greater or equal to the start number. Note that a length-0 array is valid.
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public int[] FizzArray3(int start, int end)
        {
            int len = end - start, j = 0;
            int[] arr = new int[len];
            for (int i = start; i < end; i++)
            {
                arr[j] = i;
                j++;
            }
            return arr;
           //     throw new NotImplementedException();
        }

        /// <summary>
        /// The method will simply return a new subarray that is in the original array, containing values from the first index to the last index.
        /// </summary>
        /// <param name="array"></param>
        /// <param name="firstIndex"></param>
        /// <param name="lastIndex"></param>
        /// <example>If your array is { 2, 4, 6, 8, 10, 1, 3, 5, 7} and if the first index is 3 and the last index is 5, the method will return a new array, {8, 10, 1}. </example>
        /// <returns>the subarray</returns>
        public int[] Subarray(int[] array, int firstIndex, int lastIndex)
        {

            int[] sub;
            if (array == null)
            {
                sub = null;
            }
            else
            {
                int len = lastIndex - firstIndex + 1;
                sub = new int[len];
                for (int i = 0; i < len; i++)
                {
                    sub[i] = array[i + firstIndex];
                }

            }
            return sub;
            //throw new NotImplementedException();
        }

        /// <summary>
        /// This method will simply create and return a copy of the original array by calling the Subarray method. 
        /// </summary>
        /// <param name="array"></param>
        /// <seealso cref="Subarray(int[], int, int)"/>
        /// <returns></returns>
        public int[] Copyarray(int[] array)
        {
            int[] sub;
            if (array == null)
            {
                sub = null;
            }
            else
            {
                
                sub = new int[array.Length];
                for (int i = 0; i < array.Length; i++)
                {
                    sub[i] = array[i];
                }

            }
            return sub;
            // throw new NotImplementedException();
        }

        /// <summary>
        /// Find the minimum value of the array
        /// </summary>
        /// <param name="array"></param>
        /// <returns>min value, or if the array is null or empty, simply return 0</returns>
        public int MinValue(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                return 0;
            }
            int minimum = array[0];

            if (array.Length > 0)
            {
                for (int i = 0; i < array.Length; i++)
                {
                    if (array[i] < minimum)
                    {
                        minimum = array[i];
                    }


                }
                return minimum;
            }
            return minimum;
            //throw new NotImplementedException();
        }

        /// <summary>
        /// Find the maximum value of the array
        /// </summary>
        /// <param name="array"></param>
        /// <returns>the last element, or 0 if the array is empty or null</returns>
        public int MaxValue(int[] array)
        {
            if (array == null || array.Length==0)
            {                
                return 0;
            }
            int maximum = array[0];

             if (array.Length > 0)
            {
                for (int i = 0; i < array.Length; i++)
                {
                    if (array[i] > maximum)
                    {
                        maximum = array[i];
                    }
                }
                return maximum;
            }
            return maximum;
        }

        /// <summary>
        /// Find an element in the array.
        /// </summary>
        /// <param name="array"></param>
        /// <param name="item"></param>
        /// <returns>Index of the element; -1 if not found.</returns>
        public int NeedleInHaystack(int[] array, int item)
        {
            if (array == null || array.Length == 0) return -1;
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == item) return i;
            }
            return -1;
            //throw new NotImplementedException();
        }

        /// <summary>
        /// Find the int that's closest to your item
        /// </summary>
        /// <param name="array"></param>
        /// <param name="item"></param>
        /// <returns>The value of the element that's closest to yours. 0 if not found.</returns>
        public int ClosestItem(int[] array, int item)
        {
            int closest = 0;
            if (array != null && array.Length >= 1)
            {
                int diff;
                int minDiff = Math.Abs(item - array[0]);
                closest = array[0];
                for (int i = 1; i < array.Length; i++)
                {
                    if (array[i] == item)
                    {
                        return item;
                    }
                    if (array[i] > item)
                    {
                        diff = array[i] - item;
                    }
                    else
                    {
                        diff = item - array[i];
                    }

                    if (diff < minDiff)
                    {
                        minDiff = diff;
                        closest = array[i];
                    }

                }
            }
                return closest;

                //throw new NotImplementedException();
            }

        /// <summary>
        /// Returns the farthest item from the array.
        /// </summary>
        /// <param name="array"></param>
        /// <param name="item"></param>
        /// <returns>The value of the element that's the farthest from your item; 0 if the array is null or empty</returns>
        public int FarthestItem(int[] array, int item)
        {
            int distance = 0, result = 0, max = 0;
            if (array == null || array.Length == 0) return 0;
            for (int i = 0; i < array.Length; i++)
            {
                distance = Math.Abs(array[i] - item);
                if (distance > max)
                {
                    max = Math.Max(max, distance);
                    result = i;
                }
            }
            return array[result];
            //throw new NotImplementedException();
        }

        /// <summary>
        /// Return an array that contains exactly the same numbers as the given array, but rearranged so that every 3 is immediately followed by a 4. Do not move the 3's, but every other number may move. The array contains the same number of 3's and 4's, every 3 has a number after it that is not a 3, and a 3 appears in the array before any 4.
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        public int[] Fix34(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == 3)
                {
                    for (int j = 0; j < array.Length; j++)
                    {
                        if (array[j] == 4)
                        {
                            if (j > 0 && array[j - 1] != 3)
                            {
                                int tmp = array[i + 1];
                                array[i + 1] = 4;
                                array[j] = tmp;
                            }
                            else if (j == 0)
                            {
                                int tmp = array[i + 1];
                                array[i + 1] = 4;
                                array[j] = tmp;
                            }
                        }
                    }

                }
            }
            return array;


        }
           // throw new NotImplementedException();
        

        /// <summary>
        /// (This is a slightly harder version of the fix34 problem.) Return an array that contains exactly the same numbers as the given array, but rearranged so that every 4 is immediately followed by a 5. Do not move the 4's, but every other number may move. The array contains the same number of 4's and 5's, and every 4 has a number after it that is not a 4. In this version, 5's may appear anywhere in the original array.
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        public int[] Fix45(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == 4)
                {
                    for (int j = 0; j < array.Length; j++)
                    {
                        if (array[j] == 5)
                        {
                            if (j > 0 && array[j - 1] != 4)
                            {
                                int tmp = array[i + 1];
                                array[i + 1] = 5;
                                array[j] = tmp;
                            }
                            else if (j == 0)
                            {
                                int tmp = array[i + 1];
                                array[i + 1] = 5;
                                array[j] = tmp;
                            }
                        }
                    }

                }
            }
              return array;
            // throw new NotImplementedException();
        }

        /// <summary>
        /// Say that a "clump" in an array is a series of 2 or more adjacent elements of the same value.Return the number of clumps in the given array.
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        public int CountClumps(int[] array)
        {
            int clumps = 0;
            for (int i = 0; i < array.Length - 1; i++)
            {
                if (array[i] == array[i + 1])
                    clumps++;
                for (int j = i + 1; j < array.Length; j++)
                {
                    if (array[j] == array[i])
                        i++;
                    else
                        break;
                }
            }
            return clumps;
            //throw new NotImplementedException();
        }
        #endregion

        #region Nested Loops

        /// <summary>
        /// Given n>=0, create an array length n*n with the following pattern, shown here for n=3 : {0, 0, 1,    0, 2, 1,    3, 2, 1} (spaces added to show the 3 groups).
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public int[] SquareUp(int number)
        {
            int[] arr = new int[number * number];
            for (int i = 1, k = 0; i <= number; i++)
            {
                for (int j = i; j < number; j++)
                {
                    arr[k] = 0;
                    k++;
                }
                for (int j = i; j >= 1; j--)
                {
                    arr[k] = j;
                    k++;
                }
            }
            return arr;
           // throw new NotImplementedException();
        }

        /// <summary>
        /// Given n>=0, create an array with the pattern {1,    1, 2,    1, 2, 3,   ... 1, 2, 3 .. n} (spaces added to show the grouping). Note that the length of the array will be 1 + 2 + 3 ... + n, which is known to sum to exactly n*(n + 1)/2.
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public int[] SeriesUp(int number)
        {
            int[] arr = new int[number * (number + 1) / 2];
            for (int i = 1, k = 0; i <= number; i++)
            {
                for (int j = 1; j <= i; j++)
                {
                    arr[k] = j;
                    k++;
                }
            }
            return arr;
            // throw new NotImplementedException();
        }

        /// <summary>
        /// Given a non-empty array, return true if there is a place to split the array so that the sum of the numbers on one side is equal to the sum of the numbers on the other side.
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        public bool CanBalance(int[] array)
        {
            int lSum = 0;
  for (int i = 0; i < array.Length; i++)
            {lSum += array[i];
    int rSum = 0;
  for (int j = array.Length - 1; j > i; j--)
                {rSum += array[j];
    }if (rSum == lSum)
     return true;
      
  }
  return false;
            //  throw new NotImplementedException();
        }
        #endregion
    }
}
