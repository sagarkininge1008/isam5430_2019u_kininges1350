﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW1_Account_Summary
{
    class AccountTest
    {
        static void Main()
        {
            Account account1 = new Account("Sagar ", 50.00m);
            Account account2 = new Account("Kininge", -7.53m);

            Console.WriteLine($"{account1.Name}'s balance: {account1.Balance:C}");
            Console.WriteLine($"{account2.Name}'s balance: {account2.Balance:C}");


            Console.Write("\nEnter withdrawal amount ");
            decimal debit = decimal.Parse(Console.ReadLine());
            Console.WriteLine($"Debit amount is {debit:c}\n");
            account1.Debit(debit);

            Console.WriteLine($"{account1.Name}'s balance: {account1.Balance:C}");

            Date testDate = new Date(10, 27, 1994);
            Console.WriteLine($"The date is:{testDate.DisplayDate()}");

            Student studentdetail = new Student("Sagar", "Kininge", 3.22);
            Console.WriteLine($"The student detail is :{studentdetail.stufirstname} {studentdetail.stulastname} {studentdetail.stuGPA}");
            Console.ReadLine();

        }
        static void DisplayAccount(Account accounttoDisplay)
        {
            Console.WriteLine($"{accounttoDisplay.Name}'s balance: {accounttoDisplay.Balance:C}");
        }




    }
}
