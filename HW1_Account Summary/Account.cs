﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW1_Account_Summary
{
    class Account
    {

        public string Name { get; set; }
        private decimal balance;



        public Account(string accountName, decimal intialBalance)
        {
            Name = accountName;
            balance = intialBalance;
        }

        public decimal Balance
        {
            get
            {
                return balance;
            }
            private set
            {
                if (value > 0.0m)
                {
                    balance = value;
                }
            }

        }



        public void Deposit(decimal depositAmount)
        {
            if (depositAmount > 0.0m)
            {
                Balance = Balance + depositAmount;
            }
        }

        public void Debit(decimal DebitAmount)
        {
            Balance = balance;
            if (DebitAmount > Balance)
            {
                Balance = Balance + 0;
                Console.WriteLine($"Debit amount exceeded account balance");

            }
            else
            {
                Balance = Balance - DebitAmount;
            }
        }
    }
}
