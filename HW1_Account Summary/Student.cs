﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW1_Account_Summary
{
    class Student
    {
        public int studentnumber { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public double GPA { get; set; }
        public string classification { get; set; }
        public string major { get; set; }

        public Student(int num, string fname, string lname, double gpa, string classi, string maj)
        {
            studentnumber = num;
            firstname = fname;
            lastname = lname;
            GPA = gpa;
            classification = classi;
            major = maj;
        }


        public string stufirstname { get; set; }
        public string stulastname { get; set; }
        public double stuGPA { get; set; }

        public Student(string sfname, string slname, double sgpa)
        {
            stufirstname = sfname;
            stulastname = slname;
            stuGPA = sgpa;

        }
    }
}
