﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW1_Account_Summary
{
    class Date
    {

        public int month { get; set; }
        public int day { get; set; }
        public int year { get; set; }

        public Date(int Month, int Day, int Year)
        {
            month = Convert.ToInt32(Month);
            day = Convert.ToInt32(Day);
            year = Convert.ToInt32(Year);
        }

        public string DisplayDate()
        {
            return month + "/" + day + "/" + year;
        }




    }
}

