﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISAM5430.HW03.Tests
{
    public class ArraysTests
    {
        private Arrays _object;
        [SetUp]
        public void Setup()
        {
            _object = new Arrays();
        }

        [Test]
        [Category("Single Loop")]
        public void FirstLast6()
        {
#if !DEBUG
            Assert.Multiple(() =>
            {
#endif
            Boolean expected, actual;

            expected = false;
            actual = _object.FirstLast6(null);
            Assert.AreEqual(expected, actual);

            expected = false;
            actual = _object.FirstLast6(new int[0]);
            Assert.AreEqual(expected, actual);

            expected = true;
            actual = _object.FirstLast6(new[] { 1, 2, 6 });
            Assert.AreEqual(expected, actual);

            expected = true;
            actual = _object.FirstLast6(new[] { 6, 1, 2, 3 });
            Assert.AreEqual(expected, actual);

            expected = false;
            actual = _object.FirstLast6(new[] { 13, 6, 1, 2, 3 });
            Assert.AreEqual(expected, actual);

            expected = true;
            actual = _object.FirstLast6(new[] { 13, 6, 1, 2, 6 });
            Assert.AreEqual(expected, actual);

            expected = false;
            actual = _object.FirstLast6(new[] { 3, 2, 1 });
            Assert.AreEqual(expected, actual);

            expected = false;
            actual = _object.FirstLast6(new[] { 3, 6, 1 });
            Assert.AreEqual(expected, actual);

            expected = true;
            actual = _object.FirstLast6(new[] { 3, 6 });
            Assert.AreEqual(expected, actual);

            expected = true;
            actual = _object.FirstLast6(new[] { 6 });
            Assert.AreEqual(expected, actual);

            expected = false;
            actual = _object.FirstLast6(new[] { 3 });
            Assert.AreEqual(expected, actual);

            expected = true;
            actual = _object.FirstLast6(new[] { 5, 6 });
            Assert.AreEqual(expected, actual);

            expected = false;
            actual = _object.FirstLast6(new[] { 5, 5 });
            Assert.AreEqual(expected, actual);

            expected = true;
            actual = _object.FirstLast6(new[] { 1, 2, 3, 4, 6 });
            Assert.AreEqual(expected, actual);

            expected = false;
            actual = _object.FirstLast6(new[] { 1, 2, 3, 4 });
            Assert.AreEqual(expected, actual);
#if !DEBUG
            });
#endif
        }

        [Test]
        [Category("Single Loop")]
        public void FizzArray3()
        {
#if !DEBUG
                    Assert.Multiple(() =>
                    {
#endif
            Int32[] expected, actual;

            expected = new[] { 5, 6, 7, 8, 9 };
            actual = _object.FizzArray3(5, 10);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 11, 12, 13, 14, 15, 16, 17 };
            actual = _object.FizzArray3(11, 18);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 1, 2 };
            actual = _object.FizzArray3(1, 3);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 1 };
            actual = _object.FizzArray3(1, 2);
            CollectionAssert.AreEqual(expected, actual);

            expected = new int[0];
            actual = _object.FizzArray3(1, 1);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 1000, 1001, 1002, 1003, 1004 };
            actual = _object.FizzArray3(1000, 1005);
            CollectionAssert.AreEqual(expected, actual);
#if !DEBUG
            });
#endif
        }

        [Test]
        [Category("Nested Loops")]
        public void SquareUp()
        {
#if !DEBUG
                        Assert.Multiple(() =>
                        {
#endif
            Int32[] expected, actual;

            expected = new[] { 0, 0, 1, 0, 2, 1, 3, 2, 1 };
            actual = _object.SquareUp(3);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 0, 1, 2, 1 };
            actual = _object.SquareUp(2);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 0, 0, 0, 1, 0, 0, 2, 1, 0, 3, 2, 1, 4, 3, 2, 1 };
            actual = _object.SquareUp(4);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 1 };
            actual = _object.SquareUp(1);
            CollectionAssert.AreEqual(expected, actual);

            expected = new int[0];
            actual = _object.SquareUp(0);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 2, 1, 0, 0, 0, 3, 2, 1, 0, 0, 4, 3, 2, 1, 0, 5, 4, 3, 2, 1, 6, 5, 4, 3, 2, 1 };
            actual = _object.SquareUp(6);
            CollectionAssert.AreEqual(expected, actual);
#if !DEBUG
            });
#endif
        }

        [Test]
        [Category("Nested Loops")]
        public void SeriesUp()
        {
#if !DEBUG
                            Assert.Multiple(() =>
                            {
#endif
            Int32[] expected, actual;

            expected = new[] { 1, 1, 2, 1, 2, 3 };
            actual = _object.SeriesUp(3);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 1, 1, 2, 1, 2, 3, 1, 2, 3, 4 };
            actual = _object.SeriesUp(4);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 1, 1, 2 };
            actual = _object.SeriesUp(2);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 1 };
            actual = _object.SeriesUp(1);
            CollectionAssert.AreEqual(expected, actual);

            expected = new int[0];
            actual = _object.SeriesUp(0);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 1, 1, 2, 1, 2, 3, 1, 2, 3, 4, 1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 6 };
            actual = _object.SeriesUp(6);
            CollectionAssert.AreEqual(expected, actual);
#if !DEBUG
            });
#endif
        }

        [Test]
        [Category("Single Loop")]
        public void Subarray()
        {
#if !DEBUG
                                Assert.Multiple(() =>
                                {
#endif
            int[] a = SampleArray();
            int[] actual, expected;
            expected = null;
            actual = _object.Subarray(null, 0, 0);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 2 };
            actual = _object.Subarray(a, 0, 0);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 2, 16 };
            actual = _object.Subarray(a, 0, 1);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 2, 16, 1 };
            actual = _object.Subarray(a, 0, 2);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 2, 16, 1, 14 };
            actual = _object.Subarray(a, 0, 3);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 2, 16, 1, 14, 8, 11 };
            actual = _object.Subarray(a, 0, 5);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 2, 16, 1, 14, 8, 11, 11, 6, 8 };
            actual = _object.Subarray(a, 0, 8);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 2, 16, 1, 14, 8, 11, 11, 6, 8, 17, 14, 9, 0, 4 };
            actual = _object.Subarray(a, 0, 13);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 2, 16, 1, 14, 8, 11, 11, 6, 8, 17, 14, 9, 0, 4, 19, 13, 1, 9, 7, 17, 20, 3 };
            actual = _object.Subarray(a, 0, 21);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 8 };
            actual = _object.Subarray(a, 4, 4);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 8, 11 };
            actual = _object.Subarray(a, 4, 5);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 8, 11, 11 };
            actual = _object.Subarray(a, 4, 6);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 8, 11, 11, 6 };
            actual = _object.Subarray(a, 4, 7);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 8, 11, 11, 6, 8, 17 };
            actual = _object.Subarray(a, 4, 9);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 8, 11, 11, 6, 8, 17, 14, 9, 0 };
            actual = _object.Subarray(a, 4, 12);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 8, 11, 11, 6, 8, 17, 14, 9, 0, 4, 19, 13, 1, 9 };
            actual = _object.Subarray(a, 4, 17);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 8 };
            actual = _object.Subarray(a, 8, 8);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 8, 17 };
            actual = _object.Subarray(a, 8, 9);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 8, 17, 14 };
            actual = _object.Subarray(a, 8, 10);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 8, 17, 14, 9 };
            actual = _object.Subarray(a, 8, 11);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 8, 17, 14, 9, 0, 4 };
            actual = _object.Subarray(a, 8, 13);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 8, 17, 14, 9, 0, 4, 19, 13, 1 };
            actual = _object.Subarray(a, 8, 16);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 8, 17, 14, 9, 0, 4, 19, 13, 1, 9, 7, 17, 20, 3 };
            actual = _object.Subarray(a, 8, 21);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 0 };
            actual = _object.Subarray(a, 12, 12);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 0, 4 };
            actual = _object.Subarray(a, 12, 13);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 0, 4, 19 };
            actual = _object.Subarray(a, 12, 14);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 0, 4, 19, 13 };
            actual = _object.Subarray(a, 12, 15);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 0, 4, 19, 13, 1, 9 };
            actual = _object.Subarray(a, 12, 17);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 0, 4, 19, 13, 1, 9, 7, 17, 20 };
            actual = _object.Subarray(a, 12, 20);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 1 };
            actual = _object.Subarray(a, 16, 16);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 1, 9 };
            actual = _object.Subarray(a, 16, 17);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 1, 9, 7 };
            actual = _object.Subarray(a, 16, 18);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 1, 9, 7, 17 };
            actual = _object.Subarray(a, 16, 19);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 1, 9, 7, 17, 20, 3 };
            actual = _object.Subarray(a, 16, 21);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 20 };
            actual = _object.Subarray(a, 20, 20);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 20, 3 };
            actual = _object.Subarray(a, 20, 21);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 3 };
            actual = _object.Subarray(a, 21, 21);
            CollectionAssert.AreEqual(expected, actual);




            //var sb = new StringBuilder();


            //for (int i = 0; i <= 24; i += 4)
            //{
            //    int p = 0, q = 1;
            //    if (i == 24) i = 21;
            //    for (int j = i; j < a.Length; j = i + q)
            //    {
            //        q = p + q;
            //        p = q - p;
            //        sb.AppendLine($"expected = new [] {_object.Subarray(a, i, j).ToCode()};");
            //        sb.AppendLine($"actual = _object.Subarray(a, {i}, {j});");
            //        sb.AppendLine("CollectionAssert.AreEqual(expected, actual);");
            //        sb.AppendLine();
            //    }
            //}
            //Assert.Fail(sb.ToString());
#if !DEBUG
            });
#endif
        }

        [Test]
        [Category("No Loop")]
        public void CountClumps()
        {
#if !DEBUG
            Assert.Multiple(() =>
            {
#endif
            Int32 expected, actual;

            expected = 2;
            actual = _object.CountClumps(new[] { 1, 2, 2, 3, 4, 4 });
            Assert.AreEqual(expected, actual);

            expected = 2;
            actual = _object.CountClumps(new[] { 1, 1, 2, 1, 1 });
            Assert.AreEqual(expected, actual);

            expected = 1;
            actual = _object.CountClumps(new[] { 1, 1, 1, 1, 1 });
            Assert.AreEqual(expected, actual);

            expected = 0;
            actual = _object.CountClumps(new[] { 1, 2, 3 });
            Assert.AreEqual(expected, actual);

            expected = 4;
            actual = _object.CountClumps(new[] { 2, 2, 1, 1, 1, 2, 1, 1, 2, 2 });
            Assert.AreEqual(expected, actual);

            expected = 4;
            actual = _object.CountClumps(new[] { 0, 2, 2, 1, 1, 1, 2, 1, 1, 2, 2 });
            Assert.AreEqual(expected, actual);

            expected = 5;
            actual = _object.CountClumps(new[] { 0, 0, 2, 2, 1, 1, 1, 2, 1, 1, 2, 2 });
            Assert.AreEqual(expected, actual);

            expected = 5;
            actual = _object.CountClumps(new[] { 0, 0, 0, 2, 2, 1, 1, 1, 2, 1, 1, 2, 2 });
            Assert.AreEqual(expected, actual);

            expected = 0;
            actual = _object.CountClumps(new int[0]);
            Assert.AreEqual(expected, actual);
#if !DEBUG
            });
#endif
        }

        [Test]
        [Category("No Loop")]
        public void Copyarray()
        {
#if !DEBUG
                                    Assert.Multiple(() =>
                                    {
#endif
            int[] actual, expected, a;

            expected = null;
            actual = _object.Copyarray(null);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 20 };
            a = new[] { 20 };
            actual = _object.Copyarray(a);
            Assert.AreEqual(false, ReferenceEquals(a, actual), "The copy cannot have the same reference");
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 1, 2 };
            a = new[] { 1, 2 };
            actual = _object.Copyarray(a);
            Assert.AreEqual(false, ReferenceEquals(a, actual), "The copy cannot have the same reference");
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { -13, 34, 55 };
            a = new[] { -13, 34, 55 };
            actual = _object.Copyarray(a);
            Assert.AreEqual(false, ReferenceEquals(a, actual), "The copy cannot have the same reference");
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 2, 16, 1, 14, 8, 11, 11, 6, 8, 17, 14, 9, 0, 4, 19, 13, 1, 9, 7, 17, 20, 3 };
            a = new[] { 2, 16, 1, 14, 8, 11, 11, 6, 8, 17, 14, 9, 0, 4, 19, 13, 1, 9, 7, 17, 20, 3 };
            actual = _object.Copyarray(a);
            Assert.AreEqual(false, ReferenceEquals(a, actual), "The copy cannot have the same reference");
            CollectionAssert.AreEqual(expected, actual);
#if !DEBUG
            });
#endif
        }

        private int[] SampleArray()
        {
            return new int[] { 2, 16, 1, 14, 8, 11, 11, 6, 8, 17, 14, 9, 0, 4, 19, 13, 1, 9, 7, 17, 20, 3 };
        }

        [Test]
        [Category("No Loop")]
        public void SumEqual()
        {
#if !DEBUG
            Assert.Multiple(() =>
            {
#endif
            int[] array;
            bool actual;
            int index;

            array = new int[0];
            index = 0;
            actual = _object.SumEqual(array, index);
            Assert.IsFalse(actual);

            array = null;
            index = 0;
            actual = _object.SumEqual(array, index);
            Assert.IsFalse(actual);

            array = new[] { -3, 1, 3, -1, 1, 5, 4 };
            index = -1;
            actual = _object.SumEqual(array, index);
            Assert.IsFalse(actual);

            index = 0;
            actual = _object.SumEqual(array, index);
            Assert.IsFalse(actual);

            index = 1;
            actual = _object.SumEqual(array, index);
            Assert.IsTrue(actual);

            index = 2;
            actual = _object.SumEqual(array, index);
            Assert.IsFalse(actual);

            index = 3;
            actual = _object.SumEqual(array, index);
            Assert.IsFalse(actual);

            index = 4;
            actual = _object.SumEqual(array, index);
            Assert.IsTrue(actual);

            index = 5;
            actual = _object.SumEqual(array, index);
            Assert.IsFalse(actual);

            index = 6;
            actual = _object.SumEqual(array, index);
            Assert.IsFalse(actual);

            index = 7;
            actual = _object.SumEqual(array, index);
            Assert.IsFalse(actual);
#if !DEBUG
            });
#endif
        }

        [Test]
        [Category("Single Loop")]
        public void MaxValue()
        {
#if !DEBUG
            Assert.Multiple(() =>
            {
#endif
            Int32 expected, actual;

            expected = 0;
            actual = _object.MaxValue(null);
            Assert.AreEqual(expected, actual);

            expected = 0;
            actual = _object.MaxValue(new int[0]);
            Assert.AreEqual(expected, actual);

            expected = 5;
            actual = _object.MaxValue(new[] { 1, 4, 5, 2, 2 });
            Assert.AreEqual(expected, actual);

            expected = 7;
            actual = _object.MaxValue(new[] { 6, 4, 7, 4, 2, 7, 2 });
            Assert.AreEqual(expected, actual);

            expected = 6;
            actual = _object.MaxValue(new[] { 6 });
            Assert.AreEqual(expected, actual);
#if !DEBUG
            });
#endif
        }

        [Test]
        [Category("Single Loop")]
        public void MinValue()
        {
#if !DEBUG
            Assert.Multiple(() =>
            {
#endif
            Int32 expected, actual;

            expected = 0;
            actual = _object.MinValue(null);
            Assert.AreEqual(expected, actual);

            expected = 0;
            actual = _object.MinValue(new int[0]);
            Assert.AreEqual(expected, actual);

            expected = 1;
            actual = _object.MinValue(new[] { 1, 4, 5, 2, 2 });
            Assert.AreEqual(expected, actual);

            expected = 2;
            actual = _object.MinValue(new[] { 6, 4, 7, 4, 2, 7, 2 });
            Assert.AreEqual(expected, actual);

            expected = 6;
            actual = _object.MinValue(new[] { 6 });
            Assert.AreEqual(expected, actual);
#if !DEBUG
            });
#endif
        }

        [Test]
        [Category("Single Loop")]
        public void NeedleInHaystack()
        {
#if !DEBUG
            Assert.Multiple(() =>
            {
#endif
            Int32 expected, actual;

            expected = -1;
            actual = _object.NeedleInHaystack(null, 0);
            Assert.AreEqual(expected, actual);

            expected = -1;
            actual = _object.NeedleInHaystack(new int[0], 2);
            Assert.AreEqual(expected, actual);

            expected = 0;
            actual = _object.NeedleInHaystack(new[] { 4, 5, 2, 8, 3, 7, 1 }, 4);
            Assert.AreEqual(expected, actual);

            expected = 2;
            actual = _object.NeedleInHaystack(new[] { 4, 5, 2, 8, 3, 7, 1 }, 2);
            Assert.AreEqual(expected, actual);

            expected = 6;
            actual = _object.NeedleInHaystack(new[] { 4, 5, 2, 8, 3, 7, 1 }, 1);
            Assert.AreEqual(expected, actual);

            expected = -1;
            actual = _object.NeedleInHaystack(new[] { 4, 5, 2, 8, 3, 7, 1 }, 6);
            Assert.AreEqual(expected, actual);

            expected = -1;
            actual = _object.NeedleInHaystack(new[] { 6 }, 7);
            Assert.AreEqual(expected, actual);

            expected = 0;
            actual = _object.NeedleInHaystack(new[] { 6 }, 6);
            Assert.AreEqual(expected, actual);
#if !DEBUG
            });
#endif
        }

        [Test]
        [Category("Single Loop")]
        public void ClosestItem()
        {
#if !DEBUG
            Assert.Multiple(() =>
            {
#endif
            Int32 expected, actual;

            expected = 0;
            actual = _object.ClosestItem(null, 0);
            Assert.AreEqual(expected, actual);

            expected = 0;
            actual = _object.ClosestItem(new int[0], 2);
            Assert.AreEqual(expected, actual);

            expected = 3;
            actual = _object.ClosestItem(new[] { 4, 5, 2, 8, 3, 7, 1 }, 3);
            Assert.AreEqual(expected, actual);

            expected = 1;
            actual = _object.ClosestItem(new[] { 4, 5, 2, 8, 3, 7, 1 }, 1);
            Assert.AreEqual(expected, actual);

            expected = 8;
            actual = _object.ClosestItem(new[] { 4, 5, 2, 8, 3, 7, 1 }, 9);
            Assert.AreEqual(expected, actual);

            expected = 5;
            actual = _object.ClosestItem(new[] { 4, 5, 2, 9, 3, 8, 1 }, 6);
            Assert.AreEqual(expected, actual);

            expected = 6;
            actual = _object.ClosestItem(new[] { 6 }, 6);
            Assert.AreEqual(expected, actual);

            expected = 6;
            actual = _object.ClosestItem(new[] { 6 }, -100);
            Assert.AreEqual(expected, actual);
#if !DEBUG
            });
#endif
        }

        [Test]
        [Category("Single Loop")]
        public void FarthestItem()
        {
#if !DEBUG
            Assert.Multiple(() =>
            {
#endif
            Int32 expected, actual;

            expected = 0;
            actual = _object.FarthestItem(null, 0);
            Assert.AreEqual(expected, actual);

            expected = 0;
            actual = _object.FarthestItem(new int[0], 2);
            Assert.AreEqual(expected, actual);

            expected = 7;
            actual = _object.FarthestItem(new[] { 2, 3, 7, 4, 1, 5, 6 }, 2);
            Assert.AreEqual(expected, actual);

            expected = 1;
            actual = _object.FarthestItem(new[] { 2, 3, 7, 4, 1, 5, 6 }, 5);
            Assert.AreEqual(expected, actual);

            expected = 7;
            actual = _object.FarthestItem(new[] { 2, 3, 7, 4, 1, 5, 6 }, -5);
            Assert.AreEqual(expected, actual);

            expected = 1;
            actual = _object.FarthestItem(new[] { 2, 3, 7, 4, 1, 5, 6 }, 10);
            Assert.AreEqual(expected, actual);

            expected = 3;
            actual = _object.FarthestItem(new[] { 3 }, 100);
            Assert.AreEqual(expected, actual);

#if !DEBUG
            });
#endif
        }

        [Test]
        [Category("Single Loop")]
        public void Fix34()
        {
#if !DEBUG
            Assert.Multiple(() =>
            {
#endif
            Int32[] expected, actual;

            expected = new[] { 1, 3, 4, 1 };
            actual = _object.Fix34(new[] { 1, 3, 1, 4 });
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 1, 3, 4, 1, 1, 3, 4 };
            actual = _object.Fix34(new[] { 1, 3, 1, 4, 4, 3, 1 });
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 3, 4, 2, 2 };
            actual = _object.Fix34(new[] { 3, 2, 2, 4 });
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 3, 4, 3, 4, 2, 2 };
            actual = _object.Fix34(new[] { 3, 2, 3, 2, 4, 4 });
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 2, 3, 4, 3, 4, 2, 2 };
            actual = _object.Fix34(new[] { 2, 3, 2, 3, 2, 4, 4 });
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 5, 3, 4, 5, 5, 5, 5, 5, 3, 4, 3, 4 };
            actual = _object.Fix34(new[] { 5, 3, 5, 4, 5, 4, 5, 4, 3, 5, 3, 5 });
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 3, 4, 1 };
            actual = _object.Fix34(new[] { 3, 1, 4 });
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 3, 4, 1 };
            actual = _object.Fix34(new[] { 3, 4, 1 });
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 1, 1, 1 };
            actual = _object.Fix34(new[] { 1, 1, 1 });
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 1 };
            actual = _object.Fix34(new[] { 1 });
            CollectionAssert.AreEqual(expected, actual);

            expected = new int[0];
            actual = _object.Fix34(new int[0]);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 7, 3, 4, 7, 7 };
            actual = _object.Fix34(new[] { 7, 3, 7, 7, 4 });
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 3, 4, 1, 3, 4, 1 };
            actual = _object.Fix34(new[] { 3, 1, 4, 3, 1, 4 });
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 3, 4, 1, 3, 4, 1 };
            actual = _object.Fix34(new[] { 3, 1, 1, 3, 4, 4 });
            CollectionAssert.AreEqual(expected, actual);
#if !DEBUG
            });
#endif
        }

        [Test]
        [Category("Single Loop")]
        public void Fix45()
        {
#if !DEBUG
            Assert.Multiple(() =>
            {
#endif
            Int32[] expected, actual;

            expected = new[] { 9, 4, 5, 4, 5, 9 };
            actual = _object.Fix45(new[] { 5, 4, 9, 4, 9, 5 });
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 1, 4, 5, 1 };
            actual = _object.Fix45(new[] { 1, 4, 1, 5 });
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 1, 4, 5, 1, 1, 4, 5 };
            actual = _object.Fix45(new[] { 1, 4, 1, 5, 5, 4, 1 });
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 4, 5, 4, 5, 9, 9, 4, 5, 9 };
            actual = _object.Fix45(new[] { 4, 9, 4, 9, 5, 5, 4, 9, 5 });
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 1, 1, 4, 5, 4, 5 };
            actual = _object.Fix45(new[] { 5, 5, 4, 1, 4, 1 });
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 4, 5, 2, 2 };
            actual = _object.Fix45(new[] { 4, 2, 2, 5 });
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 4, 5, 4, 5, 2, 2 };
            actual = _object.Fix45(new[] { 4, 2, 4, 2, 5, 5 });
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 4, 5, 4, 5, 2 };
            actual = _object.Fix45(new[] { 4, 2, 4, 5, 5 });
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 1, 1, 1 };
            actual = _object.Fix45(new[] { 1, 1, 1 });
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 4, 5 };
            actual = _object.Fix45(new[] { 4, 5 });
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 1, 4, 5 };
            actual = _object.Fix45(new[] { 5, 4, 1 });
            CollectionAssert.AreEqual(expected, actual);

            expected = new int[0];
            actual = _object.Fix45(new int[0]);
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 1, 4, 5, 4, 5 };
            actual = _object.Fix45(new[] { 5, 4, 5, 4, 1 });
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 4, 5, 4, 5, 1 };
            actual = _object.Fix45(new[] { 4, 5, 4, 1, 5 });
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 3, 4, 5 };
            actual = _object.Fix45(new[] { 3, 4, 5 });
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 4, 5, 1 };
            actual = _object.Fix45(new[] { 4, 1, 5 });
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 1, 4, 5 };
            actual = _object.Fix45(new[] { 5, 4, 1 });
            CollectionAssert.AreEqual(expected, actual);

            expected = new[] { 2, 4, 5, 2 };
            actual = _object.Fix45(new[] { 2, 4, 2, 5 });
            CollectionAssert.AreEqual(expected, actual);
#if !DEBUG
            });
#endif
        }

        [Test]
        [Category("Nested Loops")]
        public void CanBalance()
        {
#if !DEBUG
            Assert.Multiple(() =>
            {
#endif
            Boolean expected, actual;

            expected = true;
            actual = _object.CanBalance(new[] { 1, 1, 1, 2, 1 });
            Assert.AreEqual(expected, actual);

            expected = false;
            actual = _object.CanBalance(new[] { 2, 1, 1, 2, 1 });
            Assert.AreEqual(expected, actual);

            expected = true;
            actual = _object.CanBalance(new[] { 10, 10 });
            Assert.AreEqual(expected, actual);

            expected = true;
            actual = _object.CanBalance(new[] { 10, 0, 1, -1, 10 });
            Assert.AreEqual(expected, actual);

            expected = true;
            actual = _object.CanBalance(new[] { 1, 1, 1, 1, 4 });
            Assert.AreEqual(expected, actual);

            expected = false;
            actual = _object.CanBalance(new[] { 2, 1, 1, 1, 4 });
            Assert.AreEqual(expected, actual);

            expected = false;
            actual = _object.CanBalance(new[] { 2, 3, 4, 1, 2 });
            Assert.AreEqual(expected, actual);

            expected = true;
            actual = _object.CanBalance(new[] { 1, 2, 3, 1, 0, 2, 3 });
            Assert.AreEqual(expected, actual);

            expected = false;
            actual = _object.CanBalance(new[] { 1, 2, 3, 1, 0, 1, 3 });
            Assert.AreEqual(expected, actual);

            expected = false;
            actual = _object.CanBalance(new[] { 1 });
            Assert.AreEqual(expected, actual);

            expected = true;
            actual = _object.CanBalance(new[] { 1, 1, 1, 2, 1 });
            Assert.AreEqual(expected, actual);
#if !DEBUG
            });
#endif
        }
    }
}