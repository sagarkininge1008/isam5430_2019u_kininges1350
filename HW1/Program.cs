﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW1
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Console.WriteLine($"The character A has the value {(int)'A'}");
            Console.WriteLine($"The character B has the value {(int)'B'}");
            Console.WriteLine($"The character C has the value {(int)'C'}");
            Console.WriteLine($"The character a has the value {(int)'a'}");
            Console.WriteLine($"The character b has the value {(int)'b'}");
            Console.WriteLine($"The character c has the value {(int)'c'}");
            Console.WriteLine($"The character 0 has the value {(int)'0'}");
            Console.WriteLine($"The character 1 has the value {(int)'1'}");
            Console.WriteLine($"The character 2 has the value {(int)'2'}");
            Console.WriteLine($"The character $ has the value {(int)'$'}");
            Console.WriteLine($"The character * has the value {(int)'*'}");
            Console.WriteLine($"The character + has the value {(int)'+'}");
            Console.WriteLine($"The character / has the value {(int)'/'}");
            Console.WriteLine($"The character space has the value {(int)' '}");

        }
    }
}
